#Modulo mas importante. Es el que realmente "tira" los dados
import random as ra #Diosito Sol (?
import dice
import prechecks as pre
import re

#Una expresion regular para el manejo de un error (Que el usuario ponga
#division/multiplicacion sin parentesis)
ERROR_PATTERN = r"(?:^| )(?:(\d*)d(\d+|F|f)(?:(?: )?(\+|-)(?: )?(\d+))?(?:(?: )?(\/|\*)(?: )?(\d+))(?: (desv|vent))?)$"
COMPILED_ERROR_PATTERN = re.compile (ERROR_PATTERN)

#Tira dados normales (Los de D&D). n es la cantidad de dados que se tiran.
#Devuelve un arreglo: los primeros n elementos son los dados, mientras el
#ultimo es el resultado final
def roll_dnd (n, sides):
	if sides > 1:
		final_result = 0
		result_array = []
		m = n #No se si esto es completamente necesario, pero por si las moscas
		while m > 0:
			die = ra.randrange (1, sides + 1)
			final_result += die
			result_array.append (die)
			m -= 1
		if final_result > 0:
			result_array.append (final_result)
		return result_array
	return "Cantidad invalida de lados"

#Tira dados de Fate (Son dados de 6 caras, 2 con un +, 2 con un - y 2 con nada.
#Si tiene un +, cuenta como +1, si tiene un - como un -1 y si no tiene nada
#cuenta como 0). Normalmente solo se tiran 4, pero dejo la posibilidad a que
#pueda tirarse mas o menos (representado con una n). Al igual que la funcion
#anterior, se deja un arreglo, pero con strings
#PD: Se que redacte para el o...e, pero espero que se entienda la idea
def roll_fate (n):
	final_result = 0
	result_array = []
	m = n
	sides = ["-", "0", "+"]
	while m > 0:
		die = ra.randrange (-1, 2)
		result_array.append (sides[die + 1])
		final_result += die
		m -= 1
	if final_result > 0:
		result_array.append ("+{}".format (final_result))
	elif result_array != []:
		result_array.append (str (final_result))
	return result_array

#Tira un d20 dos veces en condicion de ventaja o desventaja (depende si cond
#esta valuado en "vent" o "desv" respectivamente). Devuelve un arreglo de tres
#valores; los primeros dos siendo las tiradas y el tercero el resultado final.
def roll_twice (cond):
	roll1 = ra.randrange (1, 21)
	roll2 = ra.randrange (1, 21)
	result = []
	if cond != "none":
		result.append (roll1)
		result.append (roll2)
		if cond == "desv":
			result.append (min (roll1, roll2))
		else:
			result.append (max (roll1, roll2))
	return result

#Aplica bonuses y divide/multiplica el resultado. Si la division resulta en un
#numero con coma, lo redondea para abajo
def apply_bonus_and_divide (base, symbol1, bonus, symbol2, operand):
	if symbol1 == "+":
		base += bonus
	else:
		base -= bonus
	if symbol2 == "*":
		return base * operand
	elif symbol2 == "/" and operand != 0:
		return base // operand
	else:
		return "No se puede dividir por 0"

#Funcion principal. Toma un string con los dados, y si es valida tira los dados
#Devuelve un arreglo de arreglos (Salvo que un error ocurra). Cada subarreglo
#representa una tirada de dados, que luego el "frontend" (El modulo que
#interactua con la API) debe procesar para mostrarle al usuario.
def roll_dice (string):
	#Checkeos previos
	if not pre.has_balanced_parenthesis (string):
		return "Parentesis desbalanceados"
	if pre.is_not_valid_list (string):
		return "La lista empieza con un espacio o termina con coma... No esta permitido!"
	#Verifica si algun dado coincide con la exp. reg. de error
	string_array = string.split (",")
	for i in string_array:
		if COMPILED_ERROR_PATTERN.match (i):
			return "No pusiste un parentesis para la division/multiplicacion"
	die_array = dice.input_to_array (string)
	error1 = "No se puede dividir por 0"
	error2 = "Cantidad invalida de dados"
	error3 = "Cantidad invalida de lados"
	result = []
	if die_array == []:
		return "Lista invalida"
	for i in die_array:
		temp_result = []
		if (i[0] != "1" or i[1] != "20") and i[6] != "none":
			return "No se puede usar ventaja/desventaja en dados que no sean 1d20"
		elif i[0] == "1" and i[1] == "20" and i[6] != "none":
			temp_result = roll_twice (i[6])
		elif i[1] == "F":
			temp_result = roll_fate (int (i[0]))
		else:
			temp_result = roll_dnd (int (i[0]), int (i[1]))
		#Primer checkeo (Comentario puesto para mejorar legibilidad)
		if error2 == temp_result:
			return error2
		elif error3 == temp_result:
			return error3
		elif temp_result == []:
			return "Cantidad invalida de dados"
		temp_result[-1] = apply_bonus_and_divide (int (temp_result [-1]), i[2], int (i[3]), i[4], int (i[5]))
		if temp_result[-1] == error1:
			return error1
		#Si son dados Fate, se querra usar strings
		if temp_result[-1] > 0 and i[1] == "F":
			temp_result[-1] = "+{}".format (temp_result[-1])
		elif i[1] == "F": #and temp_result[-1] <= 0
			temp_result[-1] = str (temp_result[-1])
		#Finalmente, se pone en el arreglo final
		result.append (temp_result)
	return result #Esto solo deberia ejecutarse si todo anduvo bien

if __name__ == "__main__":
	print ("roll_dnd & roll_fate")
	print (roll_dnd (4, 6))
	print (roll_dnd (1, 4))
	print (roll_dnd (0, 20))
	print (roll_dnd (4, 1))
	print (roll_fate (4))
	print (roll_fate (0))
	print ("roll_twice")
	print (roll_twice ("desv"))
	print (roll_twice ("vent"))
	print (roll_twice ("none"))
	print ("apply_bonus_and_divide")
	print (apply_bonus_and_divide (5, "+", 3, "*", 1))
	print (apply_bonus_and_divide (5, "-", 3, "*", 1))
	print (apply_bonus_and_divide (5, "+", 0, "/", 2))
	print (apply_bonus_and_divide (5, "+", 0, "*", 2))
	print (apply_bonus_and_divide (6, "+", 0, "/", 2))
	print (apply_bonus_and_divide (5, "+", 0, "/", 0))
	print ("roll_dice")
	print (roll_dice ("*Inserte referencia a Los Simpsons*"))
	print (roll_dice (")d20+3("))
	print (roll_dice (" d20,"))
	print (roll_dice ("d20+3 desv"))
	print (roll_dice ("(4d6+2)/2"))
	print (roll_dice ("d6 desv"))
	print (roll_dice ("4d20 vent"))
	print (roll_dice ("4dF"))
	print (roll_dice ("d20+3, 4d4+1"))
	print (roll_dice ("d0"))
	print (roll_dice ("0d20"))
	print (roll_dice ("(10d20)/0"))
	print (roll_dice ("4d6 + 3 / 2"))
