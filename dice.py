# Modulo que transforma input en json (para procesamiento)
import prechecks as pre
import re
import json

PATTERN = r"(?:^| )(?:(\()?(?P<cantidad>\d*)d(?P<dado>\d+|F|f)(?:(?: )?(?P<suma_resta>\+|-)(?: )?(?P<bonus>\d+))?(?:(\))?(?: )?(?P<multiplica_divide>\/|\*)(?: )?(?P<factor>\d+))?(?: (?P<modo>desv|vent))?)$"


def are_valid_dice(string):
    if pre.has_balanced_parenthesis(string) and not pre.is_not_valid_list(string):
        # #Variable que verifica que todas las condiciones se cumplan
        all_valid = True
        string_array = string.split(",")
        # #Verifica uno por uno los items para corroborar que sean dados validos
        for elements in string_array:
            all_valid = re.match(PATTERN, elements) is not None and all_valid
        return all_valid
    return False


def input_to_json(string):
    if are_valid_dice(string):
        string_array = string.split(",")
        for elements in string_array:
            file = open("dice.json", "a+")
            match = re.match(PATTERN, elements)
            json.dump(match.groupdict(), file, indent="\t", separators=(',', ' : '))
            file.close()


if __name__ == "__main__":
    print("are_valid_dice")
    print(are_valid_dice("d6"))  # Un item deberia dar True
    print(are_valid_dice(""))  # No se que dara un string vacio, supongo que rompera todo
    print(are_valid_dice("asdasfd"))  # Obviamente False
    print(are_valid_dice("3d6, d4+3"))  # True
    print(are_valid_dice(" 3d6,"))  # False
    print(are_valid_dice("3d6, aasdsad"))  # False
    print(are_valid_dice("(3d6-3*2"))  # False
    print(are_valid_dice("(3d6-3)*2, 3(d6)+3*2"))  # False
    print(are_valid_dice("(3d6 - 3) * 2"))  # True
    print(are_valid_dice("(3d6 - 3) * 2, 2d6"))  # True
    print("input_to_json")
    input_to_json("2d20 + 3, d20 desv, (3d6)/2")
    with open("dice.json", "r") as f:
        print(f.read())
        f.close()
