#Modulo que se encarga de hacer checkeos previos

#Funcion que verifica si un string tiene parentesis balanceados
def has_balanced_parenthesis (string):
	count = 0
	for char in string:
		if char == "(" and count >= 0:
			count += 1
		elif char == ")":
			count -= 1
	return count == 0

#Returna verdadero si empieza con un espacio o termina con coma (una lista
#valida NO debe cumplir esas condiciones)
def is_not_valid_list (string):
	if string != "":
		return string[0] == " " or string[len (string) - 1] == ","
	return False

if __name__ == "__main__":
	print ("has_balanced_parenthesis")
	print (has_balanced_parenthesis ("2d20")) #True, porque no tiene parentesis
	print (has_balanced_parenthesis ("(5d6+3)/2")) #True, porque tiene parentesis balanceados
	print (has_balanced_parenthesis ("(4d6+3), (6d4+3)"))
	print (has_balanced_parenthesis ("(5d6+3/2")) #Todos False, porque tienen parentesis desbalanceados
	print (has_balanced_parenthesis ("5d6+3)/2"))
	print (has_balanced_parenthesis (")5d6+3(/2"))
	print ("is_not_valid_list")
	print (is_not_valid_list ("d6, 5d4+3")) #False los dos (son validos)
	print (is_not_valid_list ("d6"))
	print (is_not_valid_list (" d6")) #True los dos (Son invalidos)
	print (is_not_valid_list ("d6,"))
