import roller as rol
from discord.ext import commands
import discord
import logging
from random import choice
from operator import methodcaller as method

#Diccionario de clases para la funcion "random_class_picker" (puesta fuera por
#eficiencia)
CLASSES = { "red" : "barbaro",
"magenta" : "bardo",
"gold" : "clerigo",
"green" : "druida",
"dark_red" : "guerrero",
"blue" : "monje",
"dark_gold" : "paladin",
"dark_green" : "explorador",
"dark_grey" : "picaro",
"orange" : "hechicero",
"purple" : "brujo",
"dark_blue" : "mago"}

#Mesaje de ayuda
MESSAGE= """Hola Aventurerx! Para tirar dados se debe respetar el siguiente formato:\n\n
- Los dados deben estar de la forma 'ndm', donde n es la cantidad de dados y m la cantidad de lados.\n
- n puede no estar; por defecto es 1. m, sin embargo, es obligatorio.\n
- Se puede tirar dados Fate: para eso poner 'F' como cantidad de lados.\n
- Para agregar un bonus a la tirada debe escribirse con ndm+/-b, donde b es el bonus.\n
- Para dividir o multiplicar el resultado final, debe ponerse parentesis en la tirada (por prioridad de operaciones) y luego / o * mas el operador.\n
- Para tirar con ventaja o desventaja, se debe poner *' vent'* o *' desv'* respectivamente al final de la tirada (espacio incluido).\n
- Debe tenerse en cuenta que **SOLO** se permite lanzar **UN** d20 con ventaja/desvataja. Mas dados u otra cantidad de lados causa error.\n
- Se puede tirar varios dados distintos en un mismo comando: se debe separar por una coma.\n
***EJEMPLOS VALIDOS***\n
- d20 (un dado de 20 caras)\n
- 4dF (4 dados Fate)\n
- 2d6 (2 dados de 6 caras)\n
- 2d8+3 (2 dados de 8 caras con un bonus de +3)\n
- (4d4)/2 (4 dados de 4 caras, dividido entre 2)\n
- d20 desv (tirar d20 con desventaja)\n
- 2d6, 2d8+3 (Ejemplo 2 y 3 seguidos uno del otro)\n
***EJEMPLOS INVALIDOS***\n
- 2d (Sin cantidad de lados, o cantidad invalida)\n
- 4df (La 'F' debe ser mayuscula)\n
- 2d20/2 (Debe ponerse los parentesis)\n
- 4d4 desv (Mayor cantidad de dados y/u otra cantidad de lados)\n\n
Espero que tengas una buena experiencia roleando y suerte! :)"""

#Elije un color al azar. Cada color de la lista representa una clase de D&D, en el mismo orden en que aparecen
#en el manual del jugador (version inglesa)
def random_class_picker ():
	class_color = choice (("red", "magenta", "gold", "green", "dark_red", "blue", "dark_gold", "dark_green", "dark_grey", "orange", "purple", "dark_blue"))
	actual_method = method (class_color)
	pair = (CLASSES[class_color], actual_method(discord.Colour))
	return pair

#Genera el embed a enviar
def generate_embed (title, description):
	dnd_class = random_class_picker ()
	embed = discord.Embed (title = title, description = description, color = dnd_class[1])
	embed.set_footer (text= "Tu clase es: " + dnd_class[0])
	return embed

#Sobreescribe ciertos comportamientos del comando de ayuda
class Ayuda (commands.HelpCommand):
	#Nada raro aca
	def __init__ (self):
		super().__init__()
	#Para que lo envie al DM del usario
	def get_destination (self):
		return self.context.author
	#Modificando el comando en si
	async def send_bot_help(self, mapping):
		embed = generate_embed ("Ayuda:", MESSAGE)
		await self.get_destination().send(embed=embed)

#Lee el token del archivo "secret_token" (que no esta subido al repositorio aproposito)
f = open ("secret_token", "r")
TOKEN = f.readline()
f.close()

#Snippet de codigo sacado de la documentacion. Basicamente pone el log en un
#archivo, asi lo puedo leer mas tarde
logger = logging.getLogger('discord')
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)

#Aca empieza la magia
bot = commands.Bot (command_prefix="!r", help_command=Ayuda())

@bot.event
async def on_ready():
	print ("Logged in sucessfully as {}#{}".format (bot.user.name, bot.user.id))

@bot.command()
async def hola(ctx):
	await ctx.send ("Hola Mr./Mrs. @{}".format (ctx.author))

@bot.command()
async def tirar(ctx, *, dice: str):
	rolled_dice = rol.roll_dice (dice)
	#Esto es por si hay un error
	if type (rolled_dice) is str:
		embed = generate_embed ("Error:", rolled_dice)
		await ctx.send (embed = embed)
		return
	initial_dice = dice.split (",")
	result = ""
	#Deberia haber una cantidad igual de elementos tanto en rolled_dice
	#como en initial_dice
	i = 0
	while i < len (initial_dice):
		#Verifica si el primer caracter es un espacio, porque puede ser "d20, 2d4" en
		#vez de "d20,2d4", y con el split puede quedar el espacio
		if initial_dice[i][0] != " ":
			temp_result = "*{}*: ".format (initial_dice[i])
		else:
			temp_result = "*{}*: ".format (initial_dice[i][1:])
		j = 0
		while j < len (rolled_dice[i]) - 1:
			temp_result += str (rolled_dice[i][j])
			if j < len (rolled_dice[i]) - 2:
				temp_result += ", "
			j += 1
		temp_result += " Resultado Final: **" +  str (rolled_dice[i][-1]) + "**"
		result += temp_result + "\n"
		i += 1
	embed = generate_embed ("Tirada:", result)
	await ctx.send (embed = embed)

bot.run (TOKEN)
